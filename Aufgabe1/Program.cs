﻿/// <copyright file="Program.cs" company="LettSoft">
///      Copyright (c) LettSoft. All rights reserved.
/// </copyright>
/// <summary>Programm that performs en- and decryption of text using the caesar cipher.</summary>
namespace Aufgabe1
{
    using System;

    /// <summary>
    /// The main class of the program.
    /// </summary>
    public class MainClass
    {
        /// <summary>
        /// The entry point of the program, where the program control starts and ends.
        /// </summary>
        /// <param name="args">The command-line arguments.</param>
        public static void Main(string[] args)
        {
            // Save current console colors
            ConsoleColor oldForeground = Console.ForegroundColor;
            ConsoleColor oldBackground = Console.BackgroundColor;

            // Show main menu
            MainMenu();

            // Restore console colors
            Console.ForegroundColor = oldForeground;
            Console.BackgroundColor = oldBackground;
        }

        /// <summary>
        /// Pages an array of strings to the screen. Users can advance to the next page by pressing space or enter.
        /// The pager can be quit by pressing esc or q once the last screen has been reached.
        /// </summary>
        /// <param name="lines">String array to page.</param>
        /// <param name="title">Title for the pager.</param>
        public static void PageText(string[] lines, string title)
        {
            int totalLines = 0;

            while (totalLines < lines.Length)
            {
                Console.Clear();
                DrawTextBar(title);
                Console.WriteLine();

                int maxLines = Console.WindowHeight - 1;

                // Fill the available space with as much lines as possible, if we can't fit any more then exit the loop.
                while (maxLines > 0 && totalLines < lines.Length)
                {
                    int lineheight = (int)Math.Ceiling(lines[totalLines].Length / (double)Console.WindowWidth);
                    if (maxLines - lineheight <= 0)
                    {
                        break;
                    }

                    Console.WriteLine(lines[totalLines]);
                    maxLines -= lineheight;
                    totalLines++;
                }

                if (totalLines < lines.Length)
                {
                    Console.Write("-more-");
                    WaitForKeys(ConsoleKey.Spacebar, ConsoleKey.Enter);
                }
                else
                {
                    // Console.Write("-end of file-");
                    WaitForKeys(ConsoleKey.Q, ConsoleKey.Escape);
                }
            }
        }

        /// <summary>
        /// Waits for the supplied keys.
        /// </summary>
        /// <param name="keys">One or more ConsoleKeys.</param>
        public static void WaitForKeys(params ConsoleKey[] keys)
        {
            while (true)
            {
                ConsoleKey key = Console.ReadKey(true).Key;
                if (Array.IndexOf(keys, key) > -1)
                {
                    break;
                }
            }
        }

        /// <summary>
        /// Encrypts a string using the caesar cipher.
        /// </summary>
        /// <param name="encryptionTable">The encryption table used for encryption.</param>
        public static void EncryptText(int[] encryptionTable)
        {
            Console.Clear();
            Console.CursorVisible = true;
            DrawTextBar("Enter Text to encrypt, finish with two empty lines");
            Console.WriteLine();

            string[] text = ReadLines();

            if (text.Length > 0)
            {
                for (int i = 0; i < text.Length; i++)
                {
                    text[i] = EncryptString(text[i], encryptionTable);
                }

                Console.WriteLine("Ready to encrypt your text. Press Enter to continue.");
                Console.ReadLine();

                PageText(text, "Encrypted Text. Q/ESC to exit.");
            }
            else
            {
                Console.WriteLine("Returning to menu. Press Enter to continue.");
            }
        }

        /// <summary>
        /// Encrypts a string using the caesar cipher.
        /// </summary>
        /// <param name="encryptionTable">The encryption table used for decryption.</param>
        public static void DecryptText(int[] encryptionTable)
        {
            Console.Clear();
            Console.CursorVisible = true;
            DrawTextBar("Enter Text to decrypt, finish with two empty lines");
            Console.WriteLine();

            string[] text = ReadLines();

            if (text.Length > 0)
            {
                for (int i = 0; i < text.Length; i++)
                {
                    text[i] = DecryptString(text[i], encryptionTable);
                }

                Console.WriteLine("Ready to decrypt your text. Press Enter to continue.");
                Console.ReadLine();

                PageText(text, "Decrypted Text. Q/ESC to exit.");
            }
            else
            {
                Console.WriteLine("Returning to menu. Press Enter to continue.");
            }
        }

        /// <summary>
        /// Reads multiple lines until the user enters two empty lines.
        /// </summary>
        /// <returns>String array of lines read.</returns>
        public static string[] ReadLines()
        {
            string temp = string.Empty;
            string lastline = null;

            // Read lines in a loop
            while (true)
            {
                string newline = Console.ReadLine();

                // If two empty lines were entered, return the lines in an array
                if (newline == string.Empty && lastline == string.Empty)
                {
                    if (temp.Length > 2)
                    {
                        temp = temp.Remove(temp.Length - 2); // Remove the last two empty lines
                    }
                    else
                    {
                        return new string[0];
                    }

                    return temp.Split('\n');
                }
                else
                {
                    lastline = newline;
                    temp += newline;
                    temp += "\n";
                }
            }
        }

        /// <summary>
        /// Shows the encryption table for the caesar cipher.
        /// The output is paged, press Enter or Space to continue to the next page.
        /// Press Q or Esc to exit
        /// </summary>
        /// <param name="encryptionTable">The encryption table.</param>
        public static void ShowEncryptionTable(int[] encryptionTable)
        {
            int columns = (int)Math.Ceiling((255 - 32) / (double)23); // How many columns are there in total
            int maxColumns = Console.WindowWidth / 13; // How many columns can fit onto a single screen

            int rows = 255 - 32; // How many rows are there

            int screens = (int)Math.Ceiling((double)rows / (maxColumns * 23)); // How many screens are needed for all the rows

            int character = 32; // Start at the space character

            for (int screen = 0; screen < screens; screen++)
            {
                Console.Clear();
                DrawTextBar($"Encryption Table Page {screen + 1} of {screens}");

                for (int column = 0; column < maxColumns; column++)
                {
                    Console.SetCursorPosition(column * 12, 1);
                    Console.WriteLine("Raw -> Enc");
                    for (int j = 0; j < Console.WindowHeight - 2; j++)
                    {
                        if (character >= 255)
                        {
                            column = 9999; // break outer loop as well
                            break;
                        }

                        // Skip unprintable characters
                        if (character > 126 && character <= 160)
                        {
                            character += 160 - 126;
                        }

                        Console.SetCursorPosition(column * 12, 2 + j);
                        Console.Write($"  {(char)character} -> {(char)encryptionTable[character]}");
                        character++;
                    }
                }

                if (screen < screens)
                {
                    while (true)
                    {
                        // Wait for esc or q to exit or enter to continue
                        ConsoleKey key = Console.ReadKey().Key;
                        if (key == ConsoleKey.Escape || key == ConsoleKey.Q)
                        {
                            return;
                        }
                        else if (key == ConsoleKey.Enter || key == ConsoleKey.Spacebar)
                        {
                            break;
                        }
                    }
                }
            }

            // Wait for esc or q to exit the screen
            WaitForKeys(ConsoleKey.Q, ConsoleKey.Escape);
        }

        /// <summary>
        /// Shows the decryption table for the caesar cipher.
        /// The output is paged, press Enter or Space to continue to the next page.
        /// Press Q or Esc to exit
        /// </summary>
        /// <param name="encryptionTable">The encryption table.</param>
        public static void ShowDecryptionTable(int[] encryptionTable)
        {
            int columns = (int)Math.Ceiling((255 - 32) / (double)23); // How many columns are there in total
            int maxColumns = Console.WindowWidth / 13; // How many columns can fit onto a single screen

            int rows = 255 - 32; // How many rows are there
             
            int screens = (int)Math.Ceiling((double)rows / (maxColumns * 23)); // How many screens are needed for all the rows

            int character = 32; // Start at the space character

            for (int screen = 0; screen < screens; screen++)
            {
                Console.Clear();
                DrawTextBar($"Encryption Table Page {screen + 1} of {screens}");

                for (int column = 0; column < maxColumns; column++)
                {
                    Console.SetCursorPosition(column * 12, 1);
                    Console.WriteLine("Raw -> Enc");
                    for (int j = 0; j < Console.WindowHeight - 2; j++)
                    {
                        if (character >= 255)
                        {
                            column = 9999; // break outer loop as well
                            break;
                        }

                        // Skip unprintable characters
                        if (character > 126 && character <= 160)
                        {
                            character += 160 - 126;
                        }

                        Console.SetCursorPosition(column * 12, 2 + j);
                        Console.Write($"  {(char)encryptionTable[character]} -> {(char)character}");
                        character++;
                    }
                }

                if (screen < screens)
                {
                    // Wait for esc or q to exit or enter to continue
                    while (true)
                    {
                        ConsoleKey key = Console.ReadKey().Key;
                        if (key == ConsoleKey.Escape || key == ConsoleKey.Q)
                        {
                            return;
                        }
                        else if (key == ConsoleKey.Enter || key == ConsoleKey.Spacebar)
                        {
                            break;
                        }
                    }
                }
            }

            // Wait for esc or q to exit the screen
            WaitForKeys(ConsoleKey.Q, ConsoleKey.Escape);
        }

        /// <summary>
        /// The main menu of the application. Use the arrow keys to highlight an option and press enter to select it.
        /// </summary>
        public static void MainMenu()
        {
            int offset = 13;
            string[] menuOptions = { "Encrypt Text", "Decrypt Text", "Encryption Table", "Decryption Table", "Set Cipher Offset", "Quit" };
            string[] menuDescriptions =
            {
                "Encrypt a text", "Decrypt a text", "Show the encryption table", "Show the decryption Table",
                $"Set the offset used in the cipher. Offset = {offset}", "Quit the program"
            };

            int[] encryptionTable = GenerateEncryptionTable(offset);

            while (true)
            {
                int chosen = ShowMenu("Main Menu", menuOptions, menuDescriptions);

                switch (chosen)
                {
                    case 0:
                        EncryptText(encryptionTable);
                        break;
                    case 1:
                        DecryptText(encryptionTable);
                        break;
                    case 2:
                        ShowEncryptionTable(encryptionTable);
                        break;
                    case 3:
                        ShowDecryptionTable(encryptionTable);
                        break;
                    case 4:
                        offset = AskForOffset(offset);
                        menuDescriptions[4] = $"Set the offset used in the cipher. Offset = {offset}"; // Update description with new offset
                        encryptionTable = GenerateEncryptionTable(offset);
                        break;
                    case 5:
                        if (AskYesNo("Quitting", "Do you really want to quit?"))
                        {
                            Console.WriteLine("Goodbye.");
                            return;
                        }

                        break;
                }
            }
        }

        /// <summary>
        /// Asks the user a yes/no question. (y/n) is automatically added to the string.
        /// </summary>
        /// <returns><c>true</c>, if yes was answered, <c>false</c> otherwise.</returns>
        /// <param name="title">Title of the question.</param>
        /// <param name="question">Main text of the question.</param>
        public static bool AskYesNo(string title, string question)
        {
            Console.Clear();
            DrawTextBar(title);
            Console.WriteLine();

            Console.Write(question + " (y/n)");

            while (true)
            {
                string input = Console.ReadLine();

                switch (input.ToLower())
                {
                    case "y":
                    case "yes":
                        return true;
                    case "n":
                    case "no":
                        return false;
                }

                Console.Write("Please answer y or n:");
            }
        }

        /// <summary>
        /// Prompts the user to enter a new offset for the cipher. Only positive numeric values are allowed.
        /// </summary>
        /// <returns>The new offset.</returns>
        /// <param name="offset">The current offset.</param>
        public static int AskForOffset(int offset)
        {
            Console.Clear();
            DrawTextBar("Offset Adjustment");
            Console.WriteLine();

            Console.WriteLine("The offset is used in the cipher and affects how far \"characters\" are shifted during encryption.");
            Console.WriteLine("For example, an offset of thirteen would turn \"a\" into \"n\".");
            Console.WriteLine($"The current offset is: {offset}.");
            Console.WriteLine("Please enter a new offset or enter nothing to keep the current offset.");

            while (true)
            {
                Console.Write("=>");
                string input = Console.ReadLine();

                int number;

                if (string.IsNullOrEmpty(input))
                {
                    Console.WriteLine($"Ok. I'll keep using {offset} for the offset. Press Enter to continue.");
                    Console.ReadLine();
                    return offset;
                }

                if (int.TryParse(input, out number))
                {
                    if (number <= 0)
                    {
                        Console.WriteLine("Please enter a positive number.");
                    }
                    else
                    {
                        offset = number;
                        Console.WriteLine($"The offset has been set to {offset}. Press Enter to continue.");
                        Console.ReadLine();
                        return offset;
                    }
                }
                else
                {
                    Console.WriteLine($"Error. \'{input}\' is not a number. Please enter a positive number.");
                }
            }
        }

        /// <summary>
        /// Shows a menu and allows the user to choose an entry
        /// </summary>
        /// <returns>int representing the chosen option.</returns>
        /// <param name="title">Menu title.</param>
        /// <param name="items">Menu items.</param>
        /// <param name="descriptions">Item descriptions</param>
        public static int ShowMenu(string title, string[] items, string[] descriptions)
        {
            int index = 0;

            Console.CursorVisible = false;

            while (true)
            {
                Console.BackgroundColor = ConsoleColor.Black; // Make the console clear to black
                Console.Clear();

                DrawTextBar(title, backgroundColor: ConsoleColor.Blue);
                Console.WriteLine();

                for (int i = 0; i < items.Length; i++)
                {
                    if (i == index)
                    {
                        Console.BackgroundColor = ConsoleColor.White;
                        Console.ForegroundColor = ConsoleColor.Black;
                    }

                    Console.WriteLine(items[i]);
                    DrawTextBar(descriptions[index], y: Console.WindowHeight - 1);

                    Console.BackgroundColor = ConsoleColor.Black;
                    Console.ForegroundColor = ConsoleColor.White;
                }

                ConsoleKeyInfo input = Console.ReadKey(true);

                switch (input.Key)
                {
                    case ConsoleKey.DownArrow:
                        if (index < items.Length - 1)
                        {
                            index++;
                        }

                        break;
                    case ConsoleKey.UpArrow:
                        if (index > 0)
                        {
                            index--;
                        }

                        break;
                    case ConsoleKey.Enter:
                        return index;
                    case ConsoleKey.Escape:
                        return -1;
                }
            }
        }

        /// <summary>
        /// Draws a colored bar of text.
        /// </summary>
        /// <param name="title">Content of the bar.</param>
        /// <param name="x">The x coordinate.</param>
        /// <param name="y">The y coordinate.</param>
        /// <param name="foregroundColor">Foreground color.</param>
        /// <param name="backgroundColor">Background color.</param>
        public static void DrawTextBar(string title, int x = 0, int y = 0, ConsoleColor foregroundColor = ConsoleColor.Black, ConsoleColor backgroundColor = ConsoleColor.White)
        {
            ConsoleColor oldForeground = Console.ForegroundColor;
            ConsoleColor oldBackground = Console.BackgroundColor;

            Console.BackgroundColor = backgroundColor;
            Console.ForegroundColor = foregroundColor;

            int oldX = Console.CursorLeft;
            int oldY = Console.CursorTop;

            Console.SetCursorPosition(x, y);
            Console.Write(title);

            Console.SetCursorPosition(oldX, oldY);
            Console.BackgroundColor = oldBackground;
            Console.ForegroundColor = oldForeground;
        }

        /// <summary>
        /// Performs the caesar cipher rotation on a number. Unprintable control sequences are skipped.
        /// </summary>
        /// <returns>The rotated number.</returns>
        /// <param name="number">Number to rotate.</param>
        /// <param name="amount">How much to rotate.</param>
        public static int RotateNumber(int number, int amount)
        {
            const int UnprintablesStart = 126;
            const int UnprintablesEnd = 160;

            number = (number + amount) % 255;
            if (number < 32)
            {
                number += 32;
            }

            if (number > UnprintablesStart && number < UnprintablesEnd)
            {
                number += UnprintablesEnd - UnprintablesStart;
            }

            return number;
        }

        /// <summary>
        /// Encrypts a string using the caesar cipher.
        /// </summary>
        /// <returns>The encrypted string.</returns>
        /// <param name="text">String to encrypt.</param>
        /// <param name="encryptionTable">The encryption table to use when encrypting.</param>
        public static string EncryptString(string text, int[] encryptionTable)
        {
            string temp = string.Empty;
            for (int i = 0; i < text.Length; i++)
            {
                int charIndex = (int)text[i]; // Get the characters int value

                if (charIndex > encryptionTable.Length)
                {
                    temp += text[i]; // If the character does not exist in the encryption table, just add the original
                }
                else
                {
                    temp += (char)encryptionTable[charIndex]; // Get the corresponding value from the table
                }
            }

            return temp;
        }

        /// <summary>
        /// Decrypts a string using the caesar cipher.
        /// </summary>
        /// <returns>The decrypted string.</returns>
        /// <param name="text">String to decrypt.</param>
        /// <param name="encryptionTable">The encryption table to use when decrypting.</param>
        public static string DecryptString(string text, int[] encryptionTable)
        {
            string temp = string.Empty;
            for (int i = 0; i < text.Length; i++)
            {
                int charIndex = Array.IndexOf(encryptionTable, text[i]); // Find the character in the encryption table

                if (charIndex > encryptionTable.Length)
                {
                    temp += text[i]; // If the character does not exist in the encryption table, just add the original
                }
                else
                {
                    temp += (char)charIndex; // Add its unencrypted variant to the string
                }
            }

            return temp;
        }

        /// <summary>
        /// Generates an encryption table for the given offset.
        /// </summary>
        /// <returns>The encryption table.</returns>
        /// <param name="offset">The offset.</param>
        public static int[] GenerateEncryptionTable(int offset)
        {
            int[] encryptionTable = new int[255];

            for (int i = 0; i < 255; i++)
            {
                encryptionTable[i] = i;
            }

            for (int i = 32; i < 255; i++)
            {
                if (i > 126 && i <= 160)
                {
                    continue;
                }

                encryptionTable[i] = RotateNumber(i, offset);
            }

            return encryptionTable;
        }
    }
}
